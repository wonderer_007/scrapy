# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TrailItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class StoresItem(scrapy.Item):
    
    city            = scrapy.Field()
    address         = scrapy.Field()
    country         = scrapy.Field()
    phone_number    = scrapy.Field()    
    services        = scrapy.Field()
    state           = scrapy.Field()
    store_email     = scrapy.Field()
    store_id        = scrapy.Field()
    store_image_url = scrapy.Field()
    store_name      = scrapy.Field()
    store_url       = scrapy.Field()
    weekly_ad_url   = scrapy.Field()
    zip_code        = scrapy.Field()
    store_timing    = scrapy.Field()
    store_floor_plan_url = scrapy.Field()



class ProductsItem(scrapy.Item):

    title       = scrapy.Field()
    model       = scrapy.Field()
    mpn         = scrapy.Field()
    sku         = scrapy.Field()
    upc         = scrapy.Field()
    brand       = scrapy.Field()
    title       = scrapy.Field() 
    trail       = scrapy.Field()
    rating      = scrapy.Field()
    product_url = scrapy.Field()
    description = scrapy.Field() 
    currency    = scrapy.Field() 
    retailer_id = scrapy.Field()
    image_url   = scrapy.Field()
    description = scrapy.Field()
    features    = scrapy.Field()


    current_price   = scrapy.Field()
    original_price  = scrapy.Field() 
    specifications  = scrapy.Field()
    available_instore   = scrapy.Field()
    available_online    = scrapy.Field()
    primary_image_url   = scrapy.Field()