from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from trail.items import ProductsItem
from scrapy.http import Request
import urlparse
import re


class ProductsSpider(CrawlSpider):

    name = "hhgregg"
    allowed_domains = ["hhgregg.com"]
    start_urls      = [ "http://www.hhgregg.com/"]
    rules           = [Rule(LinkExtractor(allow=['.*/item/.*']), 'parse_product_info')]
    base_url        = "http://www.hhgregg.com/"



    def parse(self, response):

        item = []
        categories_url  = response.xpath("//div[@id='header_menu_loaded']/div/a/@href").extract()

        categories_url.pop()

        for category in categories_url:
            item.append(Request(urlparse.urljoin(response.url, category),
                          callback=self.parse_category))

        return item

    def parse_category(self, response):

        item = []
        sub_categories = self.count_list_items(response.xpath("//div[@id='left_nav']/div/div/div"))

        for sub_category in range(1, sub_categories+1 ):
            sub_category_urls = response.xpath("//div[@id='left_nav']/div/div/div[%s]/ul/li/a/@href" % (str(sub_category))).extract()
            for url in sub_category_urls:
                item.append(Request(urlparse.urljoin(response.url, url),
                              callback=self.parse_sub_category))

        return item


    def parse_sub_category(self, response):

        item = []
        product_urls = response.xpath("//div[@class='grid_mode']//div[@class='item_container']//h3/a/@href").extract()

        for url in product_urls:
            item.append(Request(urlparse.urljoin(response.url, url),
                          callback=self.parse_product_info))

        return item


    def count_list_items(self, list):

        count =0
        for item in list:
            count += 1

        return count



    def absolute_url(url):

        if not self.base_url in url:
            url = "%s%s" % (self.base_url, url)

        return url


    def strip_each_item(self, list):
        for item in list:
            item = item.strip()
        return list


    def get_specifications(self, response):
    
        spec       = {}
        specification_groups = response.xpath("//div[@class='specList']/div[@class='specGroup']")

        for index in range(1, self.count_list_items(specification_groups) + 1):

            keys    = response.xpath("//div[@class='specList']/div[@class='specGroup' and position() = %s]/div[@class='specDetails']//span[@class='specdesc']//text()" % (index)).extract()
            values  = response.xpath("//div[@class='specList']/div[@class='specGroup' and position() = %s]/div[@class='specDetails']//span[@class='specdesc_right']//text()" % (index) ).extract()

            keys    = self.strip_each_item(keys)
            values  = self.strip_each_item(values)

            for key, value in zip(keys, values):
                key = key.split(":")[0]
                spec[key.strip()]   = value.strip()

        return spec


    def parse_products_link(self, response):
        print "parsing ...."
        pass

    def parse_product_info(self, response):

        title           = response.xpath("//h1[@class='catalog_link']/text()").extract()[0].strip()
        model           = response.xpath("//span[@class='model_no']/text()").extract()
        sku             = "%s_is" % (model)
        retailer_id = response.xpath("//input[@name='catEntryId']/@value").extract()

        #not sure difference between description and feature or specification

        description     = response.xpath("//div[@class='features_list']/ul/li/span/text()").extract()
        features        = response.xpath("//div[@class='features_list']/ul/li/span//text()").extract()
        specifications  = self.get_specifications(response)
        rating          = response.xpath("//span[@class='pr-rating pr-rounded average']/text()").extract()
        rating          = (float(str(rating[0])) / 5) * 100 if rating else 0

        if response.xpath("//span[@class='price spacing']/text()").extract():
            current_price   = response.xpath("//span[@class='price spacing']/text()").extract()[0].strip()
        else:
            current_price   = None


        currency = None
        if current_price:
            currency    = current_price

        mpn = None
        upc = None

        if specifications.has_key("Manufacturer Model Number"):
            mpn = specifications["Manufacturer Model Number"]

        if specifications.has_key("Product UPC"):
            upc = specifications["Product UPC"]

        if response.xpath("//div[@class='reg_price strike-through']/span[last()]/text()").extract():
            original_price  = response.xpath("//div[@class='reg_price strike-through']/span[last()]/text()").extract()[0].strip()
        else:
            original_price = None




        item = ProductsItem()
        item['title']           = title
        item['model']           = model
        item['currency']        = currency
        item['features']        = features
        item['mpn']             = mpn
        item['upc']             = upc
        item['description']     = description
        item['original_price']  = original_price
        item['current_price']   = current_price
        item['specifications']  = specifications
        item['product_url']     = response.url
        item['retailer_id']     = retailer_id
        item['rating']          = rating
        item['available_online']  = True if response.xpath("//div[@id='PDP_AddToCart']/a") else False
        item['available_instore'] = True if response.body.find("To add this item to your cart please choose a fulfillment option below") != -1 else False

        return item