from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from trail.items import StoresItem


class LocationSpider(CrawlSpider):
    
    name            = "apple"
    allowed_domains = ["apple.com"]
    start_urls      = ["http://www.apple.com/retail/storelist/"]
    rules           = [Rule(LinkExtractor(allow=['http://www.apple.com/retail/storelist/']), 'parse_stores_list', follow= True), Rule(LinkExtractor(allow=['http://www.apple.com/retail/.*']), 'parse_location')]
    days            = ['Mon', 'Tus', 'Wed', 'Thr', 'Fri', 'Sat', 'Sun']

    def parse_location(self, response):

        if response.xpath("//div[@class='store-name']/text()").extract():
            store_name      = response.xpath("//div[@class='store-name']/text()").extract()[0].strip()
        else:
            "this is not a store page"
            return 

        stree_address   = response.xpath("//div[@class='street-address']/text()").extract()[0].strip()
        locality        = response.xpath("//span[@class='locality']/text()").extract()[0].strip()
        region          = response.xpath("//span[@class='region']/text()").extract()[0].strip()
        postal_code     = response.xpath("//span[@class='postal-code']/text()").extract()[0].strip()
        phone_number    = response.xpath("//div[@class='telephone-number']/text()").extract()[0].strip()
        store_url       = response.url
        zip_code        = postal_code
        city            = locality
        state           = region
        store_image_url = response.xpath("//div[@class='store-summary']//img/@src").extract()
        country         = "US"
        store_id        = response.xpath("//meta[@name='omni_page']/@content").extract()[0].split(":")[1]


        item                = StoresItem()
        item['store_name']  = store_name
        item['address']     = "%s %s %s %s" % (stree_address, locality, region, postal_code)
        item['city']        = city
        item['state']       = state
        item['zip_code']    = zip_code
        item['phone_number']= phone_number
        item['store_url']   = store_url
        item['store_image_url'] = store_image_url
        item['country']     = country
        item['store_id']    = store_id

        regular_timing      = []
        store_timing        = {}


        for index in range(self.count(response.xpath("//table[1][@class='store-info']/tr"))):
            if index != self.count(response.xpath("//table[1][@class='store-info']/tr")) - 1:
                timings = response.xpath("//table[1][@class='store-info']/tr[last() - "+ str(index) +"]/td/text()").extract()


                time_dict   ={}
                days        = self.get_days(timings[0].split(":")[0])
                start_time, end_time = timings[1].split("-")

                for day in days:
                    time_dict['open']   = start_time.strip()
                    time_dict['close']  = end_time.strip()
                    store_timing[day.strip()] = time_dict


        item['store_timing'] = store_timing

        return item

    def get_days(self, string):

        if "-" in string:
            start, end = string.split("-")
            return self.days[self.days.index(start.strip()): self.days.index(end.strip())] if start.strip() in self.days and end.strip() in self.days else []

        return [self.days[self.days.index(string.strip())]] if string.strip() in self.days else []


    def parse_stores_list(self, response):
        print "parsing links for store information ......"
        pass

    def count(self, rows):
        count =0

        for row in rows:
            count +=1

        return count