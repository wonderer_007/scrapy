from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.http import Request,FormRequest
from trail.items import StoresItem


class LocationSpider(CrawlSpider):

    days = ['Monday', 'Tuseday', 'Wednesday', 'Thrusday', 'Friday', 'Saturday', 'Sunday']
    name = "wetseal"
    allowed_domains = ["wetseal.com"]
    start_urls      = ["http://www.wetseal.com/Stores"]


    def parse(self, response):

        params = {}        
        params['dwfrm_storelocator_findbystate'] = 'Search'

        states = response.xpath("//select[@id='dwfrm_storelocator_address_states_stateUSCA']/option/@value").extract()
        items  = []

        for state in states:
            if state:

                params['dwfrm_storelocator_address_states_stateUSCA'] = state
                items = items + [FormRequest.from_response(response,
                                    formdata=params,
                                    formxpath= "//form[@id='dwfrm_storelocator_state']",
                                    callback=self.parse_store_links)]



        return items

    def parse_store_links(self, response):

        out_links   = response.xpath("//td[@class='store-information storelocator-results-link']/a/@href").extract()
        items       = []

        while out_links:
            item = Request("http://www.wetseal.com%s" %(out_links.pop()), self.parse_store)
            items.append(item)

        return items

    def parse_store(self, response):

        item    = StoresItem()

        item['store_name']  = response.xpath("//div[@class='store-locator-details']/h1/text()").extract()[0]
        address     = response.xpath("//div[@class='store-locator-details']/p/strong/text()").extract()
        item['address']     = address

        if self.count(address) >1:

            item['city']        = address[1].split(",")[0].strip()
            state, zip_code     = address[1].split(',')[1].split(" ")
            item['zip_code']    = zip_code.strip()
            item['state']       = state.strip()
            item['country']     = "US"
    
        item['store_id']    = response.url.split("StoreID=")[1]
        item['store_url']   = response.url

        timing              = response.xpath("//div[@class='store-locator-details']/p[2]/text()").extract()
        store_timing        = {}
        

        for time in timing:
            days = self.get_days(time.split(":")[0])
            start_time, end_time = time.split(": ")[1].split("-")
            time_dict           = {}

            for day in days:
                time_dict['open']   = start_time.strip()
                time_dict['close']  = end_time.strip()
                store_timing[day.strip()] = time_dict



        item['store_timing'] = store_timing


        return item


    def get_days(self, string):

        if "-" in string:
            start, end = string.split("-")
            return self.days[self.days.index(start.strip()): self.days.index(end.strip())]

        return [self.days[self.days.index(string.strip())]]


    def count(self, list):
        count =0

        for item in list:
            count +=1

        return count