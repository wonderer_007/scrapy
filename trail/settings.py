# -*- coding: utf-8 -*-

# Scrapy settings for trail project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'trail'

SPIDER_MODULES = ['trail.spiders']
NEWSPIDER_MODULE = 'trail.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'trail (+http://www.yourdomain.com)'
